---
layout: post
title: "TWIF 28: NewPipe YouTube Client Edition"
lang: de
edition: 28
author: "Blendergeek"
//authorWebsite: "https://open.source.coffee"
fdroid: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: #0d47a1; font-style: normal; font-weight: bold;">F-Droid</em>'
featuredv1: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.5ex; box-shadow: 0.1em 0.05em 0.1em rgba(0, 0, 0, 0.3); border-radius: 1em; color: black; background: linear-gradient(orange, yellow);">Featured</em>'
featured: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: orange; font-style: normal; font-weight: bold;">Featured</em>'
major: '<em style="padding: 0.15em 0.5em 0.10em; margin-right: 0.1ex; border-style: solid; border-width: medium; border-radius: 1em; color: #8ab000; font-style: normal; font-weight: bold;">Major</em>'
number_of_updated_apps: 89
mastodonAccount: "**[@fdroidorg@mastodon.technology](https://mastodon.technology/@fdroidorg)**"
twifTag: "**[#TWIF](https://mastodon.technology/tags/twif)**"
twifThread: "[TWIF submission thread](https://forum.f-droid.org/t/twif-submission-thread)"
matrixRoom: "[#fdroid:matrix.org](https://matrix.to/#/#fdroid:matrix.org)"
telegramRoom: "https://t.me/joinchat/AlRQekvjWDTuQrCgMYSNVA"
forum: "https://forum.f-droid.org"
---


Diese Woche In F-Droid {{ page.edition }}, Woche {{ page.date | date: "%V, %G" }} <a href="{{ site.baseurl }}/feed.xml"><img src="{{ site.baseurl }}/assets/Feed-icon-16x16.png" alt="Feed"></a>

In this edition: Not too much occured this week but we do have a good number of updated apps including updates to the much loved NewPipe YouTube client and a streaming music app for YouTube too. All total there are 6 new and {{ page.number_of_updated_apps }} updated apps.
<!--more-->

[F-Droid](https://f-droid.org/) ist ein [Repository](https://f-droid.org/de/packages/) verifizierter [Free & Open Source](https://de.wikipedia.org/wiki/Free/Libre_Open_Source_Software) Android Apps, ein [Client](https://f-droid.org/app/org.fdroid.fdroid), um darauf zuzugreifen, sowie ein ganzes "App Store Set", das alle notwendigen Werkzeuge zum Einrichten und Betreiben eines App Stores bietet. Es ist ein gemeinschaftlich geführtes freies Softwareprojekt, das von einer Vielzahl von Mitwirkenden weiterentwickelt wird. Das ist ihre Geschichte der letzten Woche.


#### Hinzugefügte Apps

* [MinCal Widget](https://f-droid.org/app/cat.mvmike.minimalcalendarwidget):
  Minimal month calendar widget
* [SauceNAO](https://f-droid.org/app/com.luk.saucenao): Unofficial SauceNAO
  client
* [ForceDoze](https://f-droid.org/app/com.suyashsrijan.forcedoze): Enable
  Doze mode immediately after screen off and turn off motion sensing
* [baresip](https://f-droid.org/app/com.tutpro.baresip): baresip SIP client
* {{ page.featured }} For all people who love streaming music from YouTube,
  [MusicPiped](https://f-droid.org/app/deep.ryd.rydplayer) is just the app
  for you. This app allows you to browse youtube and stream music directly
  in a clean, material design interface.
* [Open
  Contacts](https://f-droid.org/app/opencontacts.open.com.opencontacts):
  Hide contacts away from apps stealing your contacts information


#### Aktualisierte Apps

* {{< pill major >}}
  [Rocket.Chat](https://f-droid.org/app/chat.rocket.android) version 3.0.0
  entered F-Droid this week. Messages are now stored in an offline database
  so users can view them without a network connection. Emojis received
  better support with better autocomplete and reactions can now use custom
  emojis. Security has been improved with a fix for email based 2 factor
  authentication. Other new features include per channel draft messages
  collapse/uncollapse for text attachments on messages. To see the full set
  of changes view the
  [changelog](https://github.com/RocketChat/Rocket.Chat.Android/releases).
* [OpenLauncher](https://f-droid.org/app/com.benny.openlauncher) is an
  alternative "homescreen" app for android. Version 0.6.0 fixes some issues
  with gestures while also adding more color preferences and better
  backup/restore options for settings.
* The SimpleMobileTools continue to get better. Release 5.1.1 of
  [Calendar](https://f-droid.org/app/com.simplemobiletools.calendar) brings
  pull-to-refresh for CalDAV events along with better filters and colors
  support. Like all 5.x releases of SimpleMobileTools apps, the minimum
  supported Android version is now 5.0.
* In addition to a higher minimum suported Android version,
  [Contacts](https://f-droid.org/app/com.simplemobiletools.contacts) 5.1.0
  brings an optional nickname field, an option to show only contacts with
  phone numbers, a fix for disappearing contacts and more!
* [StreetComplete](https://f-droid.org/app/de.westnordost.streetcomplete) is
  a fun, easy app for improving OpenStreetMap. Version 8.3 provides several
  enhancements for quests involving accessibility and enhances the opening
  hours form along with a few other improvements.
* [FairEmail](https://f-droid.org/app/eu.faircode.email) has seen a bit of
  development since 1.106. Several security related feautures have been
  added including encryption support, even for attachments. An advanced
  setting was also added to allow users the choice to make insecure
  connections without passwords.
* [Fennec F-Droid](https://f-droid.org/app/org.mozilla.fennec_fdroid) is a
  rebrand of Mozilla Firefox for F-Droid. Version 63 landed in F-Droid this
  week, now targeting Android Oreo. New features include picture in picture
  video support, notification channels, and a Canadian English (en-CA)
  locale.
* {{< pill featured >}} [NewPipe](https://f-droid.org/app/org.schabi.newpipe)
  is a client for the popular video streaming site YouTube. Version 0.14.2
  brought with it better layouts for tablets and a list layout for
  downloaded media. Further, users can now share subscribed channels easily
  from the longpress menu.

#### Anregungen und Rückmeldung

Haben Sie wichtige Updates einer App, über die wir schreiben sollten? Senden
Sie Ihre Tipps über [Mastodon](https://joinmastodon.org) ein! Senden Sie sie
an {{ page.mastodonAccount }} und denken Sie an einen {{ page.twifTag
}}-Tag. Oder nutzen Sie den {{ page.twifThread }} im Forum. Einsendeschluss
für das nächste TWIF ist **Donnerstag** 12:00 UTC.

Ein allgemeines Feedback kann ebenfalls über Mastodon abgegeben werden oder,
wenn Sie gerne an einem Live-Chat teilnehmen möchten, dann finden Sie uns in
**#fdroid** auf [Freenode](https://freenode.net), auf Matrix über {{
page.matrixRoom }} oder auf [Telegram]({{ page.telegramRoom }}). Alle diese
Orte werden miteinander verbunden, Sie haben also die Wahl. Sie können sich
uns auch im **[Forum]({{ page.forum }})** anschließen.
