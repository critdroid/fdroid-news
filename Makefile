REPO="https://f-droid.org/repo"
SPDX="https://raw.githubusercontent.com/spdx/license-list-data/HEAD/json/licenses.json"

all: website

userconfig.toml:
	touch userconfig.toml

website: pre-serve userconfig.toml
	awk '{if(/^[a-z]/){k=$$0;gsub("[ \t]*=.*$$","",k);if(!(k in A)){A[k]=$$0;print}}else{print}}' \
		userconfig.toml config.toml > gitlab.toml
	hugo --config gitlab.toml --i18n-warnings

serve: pre-serve
	hugo server --i18n-warnings

pre-serve: static/css/main.css i18n/strings/strings.en.json i18n/strings/strings.de.json
	echo "describe: \"$$( git describe --always )\"" > data/git.yaml
	echo "hash: \"$$( git rev-list HEAD --max-count=1 )\"" >> data/git.yaml
	echo "project: \"$$( basename $$PWD )\"" >> data/git.yaml

i18n/strings/strings.%.json: olddata/%/strings.json convertstrings.py
	mkdir -p $(dir $@)
	python3 convertstrings.py $< $@

static/css/main.css: sass/main.scss sass/*.scss
	mkdir -p $(dir $@)
	sassc $< $@

clean:
	rm -f	data/index_rewritten.json \
		data/licenses_rewritten.json \
		data/git.yaml \
		i18n/strings/strings.??.json \
		static/main.css \
		static/css/main.css \
		gitlab.toml userconfig.toml \
		content/apps/*.md
	rm -rf	public
	rmdir	i18n/strings static/css 2> /dev/null; true

# Remove everything, even things that need to be redownloaded
distclean: clean
	rm -f index-v1.json licenses.json
