{
    "misc": {
        "step_x": "Passo [number]",
        "step_x_description": "Passo [number]: [description]",
        "option_x_description": "Opzione [number]: [description]",
        "overview": {
            "tutorial_x_title": "Guida [number]: [title]",
            "title": "Cosa puoi fare con F-Droid",
            "summary": "F-Droid è un app store per Android indipendente e gestito dalla comunità, completamente libero e open source. All'interno di F-Droid puoi trovare oltre 1200 app open source, cercare ed installare app dai repository creati, o creare il tuo repository. Apri f-droid.org sul tuo dispositivo Android per scaricare l'app e iniziare!"
        }
    },
    "add_repo": {
        "no_internet": "Non hai internet? Nessun problema. Scopri <a href=\"../swap/\">come inviare e ricevere app offline.</a>",
        "title": "Come aggiungere un repo a F-Droid",
        "overview": {
            "title": "Ottieni più contenuti.",
            "summary": "Se desideri di più, aggiungi un repo a F-Droid. Un repo è una collezione di contenuti curati. Puoi trovare i repo di altre persone direttamente da loro. Gli sviluppatori solitamente linkano i loro repo F-Droid nel loro sito. Gli istruttori spesso condividono link ai repo via email, Facebook o Twitter."
        },
        "scan_qr_code": {
            "related_tutorial": "Potrebbe interessarti anche: <a href=\"../swap/\">Come inviare e ricevere app offline</a>",
            "steps": {
                "step_1": "Una volta mostrato il codice QR del repo, scansionalo con un'app scanner di QR.",
                "step_3": "Se non c'è l'opzione di aprire il link con F-Droid, chiudi l'app scanner di QR e apri l'app F-Droid.",
                "step_2": "Tocca il link. Se c'è l'opzione, apri il link usando F-Droid. Poi segui le istruzioni. Se funziona, hai finito! Altrimenti, procedi al passo successivo.",
                "step_5": "Nella schermata dei repository, tocca l'icona '+' nell'angolo in alto.",
                "step_4": "In F-Droid, tocca 'Impostazioni'. Sotto 'Mie app' seleziona 'Repository'.",
                "step_6": "L'indirizzo del repo verrà auto-compilato. Tocca 'Aggiungi'. Nota: il repo verrà salvato nei repository F-Droid, ma nessuna app verrà installata finchè non lo scegli manualmente."
            },
            "description": "Se qualcuno accanto a te ha un repo che desideri, puoi scansionare il codice QR dal suo telefono. Oppure, se il codice QR viene mostrato su un sito, scansionalo con il tuo telefono per aggiungerlo.",
            "short_summary": "Scansiona codice QR",
            "title": "Scansiona un codice QR da un altro dispositivo"
        },
        "intro": "Scannerizza i codici QR dai repo che vuoi aggiungere, dal dispositivo o dal sito dove sono stati trovati.",
        "open_repo_link": {
            "steps": {
                "step_1": "Scarica e installa l'app F-Droid sul tuo Android.",
                "step_3": "Quando hai aperto il link, tocca il pulsante 'Aggiungi a F-Droid'.",
                "step_2": "Clicca il link e aprilo in un browser web sul tuo telefono.",
                "step_5": "Verrà mostrata la finestra 'Aggiungi nuovo repository' con l'indirizzo del repo precompilato. Tocca 'Ok' per aggiungerlo.",
                "step_4": "Tocca il pulsante 'Ho F-Droid'. L'app di F-Droid verrà aperta.",
                "step_7": "Tocca un repo per vederne i contenuti.",
                "step_6": "Il tuo nuovo repo verrà aggiunto. Per vedere l'elenco dei tuoi repo, apri 'Impostazioni'. Seleziona 'Repository'.",
                "step_8": "Seleziona le app che desideri e installale sul tuo telefono."
            },
            "description": "Se un link di un repo è stato inviato via email, social o SMS, aggiungi la possibilità di installare queste app al tuo F-Droid aprendolo.",
            "short_summary": "Apri il link del repo",
            "title": "Apri il link del repo sul tuo telefono"
        }
    },
    "swap": {
        "step_1": "Apri F-Droid. Tocca 'Vicino' dal menu nella parte inferiore dello schermo.",
        "step_3": "Una volta trovate persone vicine, sia tu che il contatto dovete selezionarvi a vicenda.",
        "step_2": "Tocca 'Trova persone vicine a me'. L'app inizierà a cercare automaticamente.",
        "step_5": "Tocca il pulsante '—>'.",
        "step_4": "Una volta connessi, scegli le app che vuoi condividere con il tuo contatto.",
        "step_7": "Tocca il pulsante 'Installa' accanto alle app che vuoi installare.",
        "title": "Come inviare e ricevere app offline",
        "step_9": "Una volta completate le installazioni, le app sono disponibili nel tuo telefono Android e nell'app F-Droid nella sezione app installate.",
        "overview": {
            "title": "Invia e ricevi app offline.",
            "summary": "Non hai internet? Nessun problema. La funzione \"Vicino\" di F-Droid ti offre la possibilità di inviare e ricevere contenuti con persone nella stessa stanza."
        },
        "step_6": "Ad uno dei contatti verrà chiesto di confermare la richiesta di connessione.",
        "all_devices_need_fdroid": "Tutti i dispositivi devono avere scaricato e installato F-Droid prima di cominciare. Tutti i dispositivi devono seguire i passi qui sotto.",
        "related_tutorial": "Potrebbe interessarti anche: <a href=\"../create-repo/\">Come creare un repo</a>",
        "intro": "Non hai internet? Nessun problema. Scarica le app dalle persone accanto a te.",
        "step_8": "Tocca 'Installa' un'altra volta. Poi segui le istruzioni di Android per completare l'installazione.",
        "step_10": "Per accedere alle app installate, apri le impostazioni di F-Droid e seleziona 'Gestisci app installate' sotto 'Mie app'."
    },
    "create_repo": {
        "download_and_install": {
            "steps": {
                "step_1": "Scarica e installa la web app Repomaker sul tuo computer.",
                "step_3": "Crea un nuovo repo e dagli un nome appropriato.",
                "step_2": "Esegui la web app Repomaker. Accedi o registrati per iniziare.",
                "step_5": "Clicca il pulsante 'Aggiungi' accanto agli elementi desiderati. Quando hai finito, clicca la freccia 'indietro' per tornare al tuo repo.",
                "step_4": "Scegli di aggiungere app dalla galleria o dai repo disponibili, oppure carica i tuoi file. Clicca il pulsante 'Aggiungi dalla galleria' per scegliere da contenuti già disponibili.",
                "step_6": "Le tue app verranno mostrate nell'elenco dei contenuti del repo."
            },
            "title": "Scarica e installa Repomaker per iniziare a creare"
        },
        "title": "Come creare un repo",
        "overview": {
            "title": "Aggiungi le tue app e i tuoi file.",
            "summary": "Crea i tuoi repository personalizzati di app. Usa F-Droid per distribuire contenuti. Gli istruttori usano i repo per condividere facilmente una collezione di risorse e app con i partecipanti durante gli incontri quando non c'è una connessione internet. Gli sviluppatori usano i repo per distribuire le loro app a un pubblico di nicchia."
        },
        "share_repo": {
            "steps": {
                "step_1": "Con il repo che desideri condividere, clicca 'Condividi' nel menu. Nota: devi preparare un posto dove memorizzare il tuo repo prima di condividerlo. Trovi le istruzioni al passo 2.",
                "step_3": "Scegli il percorso che meglio si adatta alle tue esigenze. Una volta fatto, sei pronto per condividere! Torna a 'Condividi'.",
                "step_2": "Ti verrà chiesto di impostare un percorso per pubblicare il tuo repo e prima di condividerlo. Se non vedi questa pagina, continua al passo 4.",
                "step_5": "L'opzione 'Vedi repo' ti permette di vedere cos'hai appena pubblicato o condiviso.",
                "step_4": "Scegli di copiare il link e invialo direttamente alle persone via email o messaggio, oppure condividi il repo via Facebook o Twitter. Quando qualcuno apre il link al tuo repo, dovrà aggiungerlo ad un dispositivo Android per accedere alle app nel repo. Vedi <a href='../add-repo/'>Come aggiungere un repo a F-Droid</a> per maggiori informazioni."
            },
            "title": "Personalizza il tuo repo"
        },
        "update_repo": {
            "steps": {
                "step_1": "Se aggiungi app dalla galleria, vengono automaticamente aggiornate quando la fonte aggiunge nuove versioni. Per i file che hai caricato, puoi trascinare le versioni aggiornate nell'elenco del repo.",
                "step_3": "Scorri in basso per le versioni. Poi aggiungine una nuova sotto la sezione 'Cronologia'.",
                "step_2": "In alternativa, puoi aprire un elemento per aggiungere un nuova versione. Seleziona modifica.",
                "step_4": "Poi aggiungi una nuova versione sotto la sezione 'Cronologia'."
            },
            "title": "Aggiorna il tuo repo"
        },
        "related_tutorial": "Potrebbe interessarti anche: <a href='../add-repo/'>Come aggiungere un repo a F-Droid</a>",
        "intro": "Non riesci a trovare quello che cerchi? Perchè non crei la tua collezione personalizzata di app e file usando Repomaker? Scarica la web app Repomaker sul tuo computer per iniziare.",
        "customize_repo": {
            "steps": {
                "step_1": "Clicca un elemento per aprirne i dettagli.",
                "step_3": "Personalizza il contenuto per adattarlo al tuo pubblico. Nota: le modifiche effettuate saranno solo per il tuo repo. Non aggiorneranno la fonte originale. Ti preghiamo di avere buon senso modificando le informazioni per contenuti che non ti appartengono.",
                "step_2": "Clicca 'Modifica'.",
                "step_4": "Tutte le tue modifiche sono auto-salvate."
            },
            "description": "Hai la possibilità di modificare le sintesi delle app e dei file per renderle comprensibili al tuo pubblico. Puoi cambiare la lingua per renderlo specifico per nazione. Poi condividi il repo agevolmente con i partecipanti prima di conferenze e tirocini.",
            "title": "Personalizza il tuo repo"
        }
    }
}
